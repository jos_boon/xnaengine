﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using XNAEngine;
using XNAEngine.UI;
using XNAEngine.Physics;
using XNAEngine.InputHandlers;
using XNAEngine.ScreenCollection;

namespace XNAEngine.XNAConsole
{
    public delegate void Execute(GameConsole sender, string[] args);

    // TODO: Make args name array (Parameter | Description)
    public struct Command
    {
        public string name;                 // command name
        public string argsName;             // arguments name 
        public string description;          // description of the command
        public Execute execute;             // Function call for this command
    }

    public class GameConsole : Panel
    {
        //private Screen      _screen;
        private TextArea    _input;
        private TextArea    _output;

        private ScreenManager _screenManager
        {
            get { return ScreenManager.Instance; }
        }

        public List<Screen> Screens
        {
            get { return _screenManager.Screens; }
        }

        // TODO: Cleanup Input & Output input.dispose();
        public TextArea Input 
        {
            get
            {
                return _input;
            }
            set
            {
                // Remove old input
                if (_input != null)
                {
                    _input.OnEnterPressed -= OnEnterPressed;
                    this.RemoveUIComponent(_input);
                }
                    
                _input = value;

                if(_input != null)
                {
                    this.AddUIComponent(_input);
                    _input.OnEnterPressed += OnEnterPressed;
                }
            }
        }
        public TextArea Output
        { 
            get { return _output; } 
            set 
            {
                // Remove old output
                if (_output != null)
                    this.RemoveUIComponent(_output);

                _output = value;

                if(_output != null)
                {
                    this.AddUIComponent(_output);
                    _output.ReadOnly = true;
                    _output.Focus = true; // FIX: set to false
                }
            } 
        }
                
        public List<Command> Commands;

        public PhysicsWorld World
        {
            get { return _world; }
        }

        // TODO: I don't like that physics world has to be sent with it
        private GameConsole(Game game, PhysicsWorld world) : base(game, world)
        {
            Commands = new List<Command>();

            InitializeCommands();
        }

        /// <summary>
        /// Build a basic 600x400 console with two standaard textareas setup as child of the console.
        /// </summary>
        public override void Initialize()
        {
            Output.MultiLine = true;
            Output.ScrollAble = true;
            Input.Focus = true;

            Input.DrawOrder = 1;
            Output.DrawOrder = 1;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteFont f = XNAEngineContentLib.Content.Load<SpriteFont>("defaultFont"); ;
            Vector2 half = Size / 2;

            Input.TextFont = f;
            Output.TextFont = f;

            Input.Size = new Vector2(Size.X - 10, Input.FontSize.Y);
            Output.Size = Size - new Vector2(10, Output.FontSize.Y);

            Input.transform.LocalPosition = new Vector2(0, half.Y - Input.Size.Y / 2);
            Output.transform.LocalPosition = -new Vector2(0, Output.FontSize.Y) / 2;

            // Calc max lenght for word wrap
            int length = (int)(Output.Size.X / (Output.FontSize.X + f.Spacing));
            Output.WrapAtLength = length;

            // Set background color
            SetBackgroundTexture(new Color(0, 0, 0, 175));

            WriteLine("Type \'HELP\' to list the commands");
 	        base.LoadContent();
        }

        /// <summary>
        /// Calls the GameConsole constructor, input and output textarea constructor and links them together
        /// </summary>
        public static GameConsole BuildConsole(Game game, PhysicsWorld world, Screen s, Vector2 size)
        {
            GameConsole con = new GameConsole(game, world);
            con.Size = size;

            TextArea input = new TextArea(game, world);
            TextArea output = new TextArea(game, world);
            con.Input = input;
            con.Output = output;

            s.GameObjects.Add(con);
            s.GameObjects.Add(input);
            s.GameObjects.Add(output);

            return con;
        }

        private void InitializeCommands()
        {
            Command c = new Command();
            c.name = "HELP";
            c.argsName = "<command>" ;
            c.description = "List the commands with their description";
            c.execute = PrintHelp;
            Commands.Add(c);

            c = new Command();
            c.name = "MOVE";
            c.argsName = "<id> <x>, <y>";
            c.description = "Move GameObject to new location";
            c.execute = MoveGameObject;
            Commands.Add(c);

            c = new Command();
            c.name = "LIST";
            c.argsName = "<int>";
            c.description = "LIST - List of screens | LIST <index> - List of gameobjects within the screen";
            c.execute = ListCommand;
            Commands.Add(c);

            c = new Command();
            c.name = "CLEAR";
            c.argsName = "";
            c.description = "Clears the console output";
            c.execute = Clear;
            Commands.Add(c);

            c = new Command();
            c.name = "NEW";
            c.argsName = "<int>";
            c.description = "Clears gameobject list and reinitialize the screen";
            c.execute = NewScreen;
            Commands.Add(c);
        }

        private void PrintHelp(GameConsole sender, string[] args)
        {
            WriteLine("COMMAND\t\tDESCRIPTION");
            foreach(Command cmd in Commands)
            {
                WriteLine(cmd.name + "\t\t" + cmd.description);
            }
        }

        private void MoveGameObject(GameConsole sender, string[] args)
        {
            // TODO: these messages might be redundant (more commands will have similar conditions :/)
            if(args.Length <= 4)
            {
                WriteLine("ERROR: Missing arguments!");
                return;
            }

            int screenI, index, x, y;
            if (ConvertToInt(args[1], out screenI) && ConvertToInt(args[2], out index) && ConvertToInt(args[3], out x) && ConvertToInt(args[4], out y) 
                && ValidIndices(screenI, index))
            {
                Transform t = (_screenManager.Screens[screenI].GameObjects[index] as GameObject).transform;
                Vector2 temp = t.Position;
                t.LocalPosition = new Vector2(x, y);

                WriteLine(String.Format("Moved GameObject({0}) FROM {1} TO {2}.", index, temp, t.Position));
            }

        }

        private void ListCommand(GameConsole sender, string[] args)
        {
            // List the GameObjects of the specified screen
            int n = 0;
            if (args.Length > 1)
            {
                if(ConvertToInt(args[1], out n) && IndexWithInBounds(n, Screens.Count))
                {
                    Screen s = _screenManager.Screens[n];

                    for (int i = 0; i < s.GameObjects.Count; i++)
                        WriteLine(String.Format("ID: {0} - {1}", i.ToString(), s.GameObjects[i].GetType()));
                }
            }
            else // List the screens
            {
                for (int i = 0; i < _screenManager.Screens.Count; i++)
                    WriteLine(String.Format("ID: {0} - {1}", i.ToString(), _screenManager.Screens[i].GetType()));
            }
        }

        private void NewScreen(GameConsole sender, string[] args)
        {
            int i = 0;

            if (args.Length > 1 && ConvertToInt(args[1], out i) && IndexWithInBounds(i, Screens.Count))
            {
                // TODO: INDEX out of bounds check
                _screenManager.Screens[i].Empty();
                _screenManager.Screens[i].Initialize();
                WriteLine("Screen has been cleared and reinitialized!");
            }
        }

        public bool ValidIndices(int indexOfScreen, int indexOfGameObject)
        {
            if(IndexWithInBounds(indexOfScreen, _screenManager.Screens.Count))
            {
                if(IndexWithInBounds(indexOfGameObject, _screenManager.Screens[indexOfScreen].GameObjects.Count))
                    return true;
            }

            return false;
        }

        public bool IndexWithInBounds(int index, int bounds)
        {
            if (index >= 0 && index < bounds)
                return true;

            WriteLine("ERROR: Index is out of bounds!");
            return false;
        }

        public bool ConvertToInt(string number, out int n)
        {
            if(int.TryParse(number, out n))
                return true;

            WriteLine("ERROR: Failed to convert argument to INT!");
            return false;
        }

        public void WriteLine(string message)
        {
            if(_output != null)
                _output.WriteLine(message);
        }

        public void Write(string message)
        {
            if (_output != null)
                _output.Write(message);
        }

        public void Clear(GameConsole sender, string[] args)
        {
            _output.Clear();
        }

        private void OnEnterPressed(object sender, EventArgs e)
        {
            TextArea t = sender as TextArea;
            string command = t.Text;

            WriteLine(command);
            handleCommand(command);

            t.Clear();
        }

        private void handleCommand(string input)
        {
            bool commandFound = false;
            string[] args = input.Split(' ');

            foreach (Command cmd in Commands)
            {
                if(cmd.name == args[0])
                {
                    commandFound = true;
                    cmd.execute(this, args);
                    break;
                }
            }

            if (!commandFound)
                WriteLine("ERROR: UNKNOWN COMMAND!");
        }

        public void Toggle()
        {
            this.Enabled = !this.Enabled;
            this.Visible = !this.Visible;
        }

        protected override void OnEnabledChanged(object sender, EventArgs args)
        {
            _input.Enabled = !_input.Enabled;
            _output.Enabled = !_output.Enabled;

            base.OnEnabledChanged(sender, args);
        }

        protected override void OnVisibleChanged(object sender, EventArgs args)
        {
            _input.Visible = !_input.Visible;
            _output.Visible = !_output.Visible;

            base.OnVisibleChanged(sender, args);
        }

    }
}
