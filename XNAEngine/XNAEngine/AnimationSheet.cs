﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XNAEngine
{
    struct AnimationStrip
    {
        public String Name;
        public Vector2 Start;
        public Vector2 End;
        public int FramesPerSecond;
    }

    class AnimationSheet : Sprite
    {
        // TODO: Animation class is atm broken (not using init and load properly)
        private List<AnimationStrip>    _animations;
        private AnimationStrip          _currentAnimation;

        private bool _isPlaying;
        private bool _loop;
        private bool _playInReverse;

        private float _elapsedTimer;
        private float _frameTimer;
        private int   _currentFrame;

        public AnimationSheet(GameObject gObj) : base(gObj)
        {
            _animations = new List<AnimationStrip>();
            _currentAnimation = new AnimationStrip();
        }

        public void Initialize(Game game, string assetName, int frameWidth, int frameHeight)
        {
            //base.Initialize(game, transform, assetName);

            Halfsize = new Vector2(frameWidth / 2, frameHeight / 2);
            _srcRect.Width = frameWidth;
            _srcRect.Height = frameHeight;

            base.Initialize();
        }

        public void AddAnimation(string name, Vector2 start, Vector2 end, int fps)
        {
            int temp;
            if(findAnimationByName(name, out temp))
            {
                Console.WriteLine("ANIMATION ERROR! Could not add Animation with name \'{0}\' because that name already exists within the list!");
                return;
            }

            AnimationStrip animation;
            animation.Name = name;
            animation.Start = start;
            animation.End = end;
            animation.FramesPerSecond = fps;

            _animations.Add(animation);
        }

        /// <summary>
        /// Start playing an new animation or return playing with the current animation
        /// </summary>
        /// <param name="name">Name of the animation you want to play</param>
        /// <param name="loop">Loop animation or not (default = false)</param>
        /// <param name="reverse">Reverse play the animation (default = false)</param>
        public void Play(string name, bool loop = false, bool reverse = false)
        {
            if(_currentAnimation.Name != name)
            {
                int index;
                if (findAnimationByName(name, out index))
                {
                    _currentAnimation = _animations[index];
                    _frameTimer = 1.0f / _currentAnimation.FramesPerSecond;
                    _currentFrame = 0;
                    _srcRect.Y = (int)_currentAnimation.Start.Y;
                }    
            }

            // These settings can be changed while playing the same animation
            _loop = loop;
            _playInReverse = reverse;

            _isPlaying = true;
        }

        /// <summary>
        /// Pause the animation and draws the current frame of the animation
        /// </summary>
        public void Pause()
        {
            _isPlaying = false;
        }

        /// <summary>
        /// Stops the animation and draws the first frame of the animation
        /// </summary>
        public void Stop()
        {
            _isPlaying = false;

            // Return to start position of animation
            _srcRect.X = (int)_currentAnimation.Start.X;
            _srcRect.Y = (int)_currentAnimation.Start.Y;
        }

        private bool findAnimationByName(string name, out int index)
        {
            index = -1;

            for (int i = 0; i < _animations.Count; i++)
            {
                if (_animations[i].Name == name)
                {
                    index = i;
                    return true;
                }
            }

            Console.WriteLine("ANIMATION WARNING! Could not find animation with name \'{0}\'.", name);

            return false;
        }

        public override void Draw(GameTime gameTime)
        {
            // TODO: Add reverse play support
            if(_isPlaying)
            {
                _elapsedTimer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if(_elapsedTimer >= _frameTimer)
                {
                    _elapsedTimer = 0;

                    int offX = _srcRect.Width * _currentFrame;
                    _srcRect.X = offX;

                    if (offX < _currentAnimation.End.X)
                        _currentFrame++;
                    else if (_loop)
                        _currentFrame = 0;
                }
            }

            base.Draw(gameTime);
        }
    }
}
