using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XNAEngine
{
    public class Sprite
    {
        public GameObject gameObject { get; private set; }
        protected Transform _transform 
        { 
            get 
            { 
                return gameObject.transform; 
            } 
        }

        public Texture2D                texture { get; private set; }
        private SpriteBatch             _batch;
    
        protected Rectangle             _srcRect;

        public Color color
        {
            get;
            set;
        }

        public Vector2 Halfsize
        {
            get;
            protected set;
        }

        public Rectangle DestinationRectangle
        {
            get { return new Rectangle((int)_transform.Position.X, (int)_transform.Position.Y, (int)(Halfsize.X * 2 * _transform.Scale.X), (int)(Halfsize.Y * 2 * _transform.Scale.Y)); }
        }

        public Rectangle SourceRectangle
        {
            get { return _srcRect; }
        }

        public LayerDepthOrder LayerDepth { get; set; }

        public Sprite(GameObject gObj)
        {
            this.gameObject = gObj;
        }

        public void Initialize()
        {
            _batch = (SpriteBatch)gameObject.Game.Services.GetService(typeof(SpriteBatch));
            color = Color.White;
            LayerDepth = LayerDepthOrder.DefaultLayer;
        }

        public void LoadContent(string assetName, Nullable<Rectangle> source = null)
        {
            ChangeTexture(gameObject.Game.Content.Load<Texture2D>(assetName), source);
        }

        public void ChangeTexture(Texture2D texture, Nullable<Rectangle> source = null)
        {
            this.texture = texture;

            if (!source.HasValue)
                _srcRect = new Rectangle(0, 0, this.texture.Width, this.texture.Height);
            else
                _srcRect = source.Value;

            Halfsize = new Vector2(_srcRect.Width, _srcRect.Height) / 2;
        }

        public virtual void Draw(GameTime gameTime)
        {
            float rad = MathHelper.ToRadians(_transform.Rotation);
            if(_batch != null)
                _batch.Draw(this.texture, DestinationRectangle, _srcRect, color, rad, Halfsize, SpriteEffects.None, LayerDepth.Value);
        }
    }
}
