﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XNAEngine.InputHandlers
{
    /// <summary>
    /// Extending on the mouse state of XNA
    /// </summary>
    public sealed class MouseHandler : InputHandler
    {
        public MouseState PrevMouseState { get; private set;}

        public MouseHandler()
        {
            
        }

        public bool ScrollUp { get; private set; }
        public bool ScrollDown { get; private set; }

        /// <summary>
        /// Difference in value of the scroll wheel of the current minus the previous
        /// </summary>
        public int ScrollWheelValue { get; private set; }

        public ExtendButtonState ExtendLeftButton { get; private set; }
        public ExtendButtonState ExtendRightButton { get; private set; }

        public ExtendButtonState ExtendSideButtonOne { get; private set; }
        public ExtendButtonState ExtendSideButtonTwo { get; private set; }

        public bool HasMouseStateChanged { get; private set; }

        public Vector2 MousePosition
        {
            get
            {
                return new Vector2(PrevMouseState.X, PrevMouseState.Y);
            }
        }

        public override void Update(GameTime gameTime)
        {
            // Set current state to current frame;
            MouseState currentMouseState = Mouse.GetState();

            // State has changed
            if (!currentMouseState.Equals(PrevMouseState))
                HasMouseStateChanged = true;
            else
                HasMouseStateChanged = false;

            // Set state of buttons
            ExtendLeftButton = setState(PrevMouseState.LeftButton, currentMouseState.LeftButton);
            ExtendRightButton = setState(PrevMouseState.RightButton, currentMouseState.RightButton);
            ExtendSideButtonOne = setState(PrevMouseState.XButton1, currentMouseState.XButton1);
            ExtendSideButtonTwo = setState(PrevMouseState.XButton2, currentMouseState.XButton2);

            // Set Scroll state            
            ScrollUp = false;
            ScrollDown = false;

            if (currentMouseState.ScrollWheelValue > PrevMouseState.ScrollWheelValue)
                ScrollUp = true;
            else if (currentMouseState.ScrollWheelValue < PrevMouseState.ScrollWheelValue)
                ScrollDown = true;

            ScrollWheelValue = currentMouseState.ScrollWheelValue - PrevMouseState.ScrollWheelValue;

            // Set previous state to current frame;
            PrevMouseState = currentMouseState;

            base.Update(gameTime);
        }
    }
}
