using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace XNAEngine.InputHandlers
{
    /// <summary>
    /// Extending on the keyboard state of XNA
    /// </summary>
    // TODO: Loop into the possibilities of the XNA keyboard state
    public sealed class KeyboardHandler : InputHandler
    {
        private KeyboardState _prevKeyboardState;
        private KeyboardState _curKeyboardState;

        public bool HasKeyboardStateChanged
        {
            get;
            private set;
        }

        public Keys[] GetPressedKeys
        {
            get
            {
                return _curKeyboardState.GetPressedKeys();
            }
        }

        public KeyboardHandler()
        {
            
        }

        public override void Update(GameTime gameTime)
        {
            _prevKeyboardState = _curKeyboardState;
            _curKeyboardState = Keyboard.GetState();

            if (!_prevKeyboardState.Equals(_curKeyboardState))
                HasKeyboardStateChanged = true;
            else
            {
                HasKeyboardStateChanged = false;
                return;
            }

            base.Update(gameTime);
        }

        public ExtendButtonState getExtendedKeyState(Keys key)
        {
            return setState(BoolToState(_prevKeyboardState, key), BoolToState(_curKeyboardState, key));
        }

        public ButtonState getKeyState(Keys key)
        {
            return BoolToState(_curKeyboardState, key);
        }

        private ButtonState BoolToState(KeyboardState state, Keys key)
        {
            if (state.IsKeyDown(key))
                return ButtonState.Pressed;
            else
                return ButtonState.Released;
        }
        
    }
}
