﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XNAEngine.InputHandlers
{
    abstract public class InputHandler : IUpdateable
    {
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;

        public bool Enabled
        {
            get;
            set;
        }
        public int UpdateOrder
        {
            get;
            set;
        }

        public InputHandler()
        {
        }

        /// <summary>
        /// Update the state of the mousebutton
        /// </summary>
        /// <param name="prev">previous frame state of the button</param>
        /// <param name="cur">current frame state of the button</param>
        /// <returns></returns>
        protected ExtendButtonState setState(ButtonState prev, ButtonState cur)
        {
            // Button has just been released
            if (prev == ButtonState.Pressed && cur == ButtonState.Released)
                return ExtendButtonState.JustReleased;

            // Button has just been pressed
            if (cur == ButtonState.Pressed && prev == ButtonState.Released)
                return ExtendButtonState.JustPressed;

            // Button is hold down
            if (prev == ButtonState.Pressed && cur == ButtonState.Pressed)
                return ExtendButtonState.Holding;

            // Button has not been pressed
            return ExtendButtonState.Released;
        }

        public virtual void Update(GameTime gameTime)
        {
            
        }
 
    }

    // TODO: Holding is now true after 2 frames (too fast?)
    public enum ExtendButtonState
    {
        Released,
        JustPressed,
        JustReleased,
        Holding
    }
}
