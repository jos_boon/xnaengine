using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNAEngine.InputHandlers
{
    /// <summary>
    /// Input manager that keeps track of the Mouse and Keyboard input
    /// </summary>
    // TODO: Singleton in use but if forgotten to add to the Game.Components list. It will then fail to update the mouse and keyboard handlers
    public sealed class InputManager : IGameComponent, IUpdateable
    {
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;

        public bool Enabled
        {
            get;
            set;
        }

        public int UpdateOrder
        {
            get;
            set;
        }

        private static InputManager _instance;
        public static InputManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new InputManager();

                return _instance;
            }
        }

        private MouseHandler _mouseHandler;
        private KeyboardHandler _keyboardHandler;

        public static MouseHandler mouseHandler 
        { 
            get 
            { 
                return Instance._mouseHandler; 
            } 
        }

        public static KeyboardHandler keyboardHandler
        {
            get 
            {
                return Instance._keyboardHandler;
            }
        }

        private InputManager()
        {
            _mouseHandler = new MouseHandler();
            _keyboardHandler = new KeyboardHandler();
        }

        public void Initialize()
        {
            Enabled = true;
        }

        public void Update(GameTime gameTime)
        {
            _mouseHandler.Update(gameTime);
            _keyboardHandler.Update(gameTime);
        }
    }
}
