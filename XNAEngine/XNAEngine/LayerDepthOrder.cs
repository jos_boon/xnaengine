﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAEngine
{
    // TODO: Create a list to dynamicly add new layers
    public class LayerDepthOrder
    {
        public string Name { get; private set; }
        public float Value { get; private set; }

        private LayerDepthOrder(string name, float value)
        {
            Name = name;
            Value = value;
        }

        public override string ToString()
        {
            return Name;
        }

        public static readonly LayerDepthOrder DebugFontLayer   = new LayerDepthOrder("DebugFontLayer", 0.0f);
        public static readonly LayerDepthOrder DebugLayer       = new LayerDepthOrder("DebugLayer", 0.1f);
        public static readonly LayerDepthOrder FrontLayer       = new LayerDepthOrder("FrontLayer", 0.2f);
        public static readonly LayerDepthOrder DefaultLayer     = new LayerDepthOrder("DefaultLayer", 0.5f);
        public static readonly LayerDepthOrder BackgroundLayer  = new LayerDepthOrder("BackgroundLayer", 1.0f);

        // Add your own layers below
        public static readonly LayerDepthOrder PlayerLayer      = new LayerDepthOrder("PlayerLayer", 0.4f);
    }
}
