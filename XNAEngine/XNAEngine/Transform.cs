﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XNAEngine
{
    public class Transform
    {
        public readonly GameObject gameObject;

        public Transform Parent
        {
            get 
            {
                if (gameObject.Parent != null)
                    return gameObject.Parent.transform;
                else 
                    return null;
            }
        }

        private Vector2     _scale;
        private Vector2     _position;
        private Rectangle   _destRect;
        private float       _rotation;

        public Vector2 Up
        {
            get
            {
                float rad = MathHelper.ToRadians(Rotation);
                Vector2 up = new Vector2(0, -1);

                up = Vector2.Transform(up, Matrix.CreateRotationZ(rad));

                return up;
            }
        }
        public Vector2 Right    
        { 
            get
            {
                float rad = MathHelper.ToRadians(Rotation);
                Vector2 right = new Vector2(1, 0);

                right = Vector2.Transform(right, Matrix.CreateRotationZ(rad));

                return right;
            }
        }

        // TODO: Local and Global positions (these aren't updated correctly either)
        /// <summary>
        /// Retreive the global position of this object
        /// </summary>
        public Vector2 Position
        {
            get
            {
                if (Parent != null)
                    return Parent.Position + _position;
                else
                    return _position;
            }
        }

        /// <summary>
        /// Retrieve the local position of this object
        /// </summary>
        public Vector2 LocalPosition
        {
            get { return _position; }
            set 
            { 
                _position = value;

                _destRect.X = (int)(Position.X);
                _destRect.Y = (int)(Position.Y);
            }
        }

        public Vector2 Scale
        {
            get
            {
                return _scale;
            }
            set
            {
                _scale = value;
                
                //TODO: Scaling should affect Colliders, ... (add more if needed).
                //_destRect.Width = (int)(HalfSize.X * _scale.X);
                //_destRect.Height = (int)(HalfSize.Y * _scale.Y);

            }
        }

        public float Rotation
        {
            get 
            { 
                return _rotation;
            }
            set
            {
                _rotation = value;

                if (gameObject.collider != null)
                    gameObject.collider.ReInitialize();
            }
        }

        /// <summary>
        /// Return Destination Rectangle
        /// </summary>
        //public Rectangle DestRect
        //{
        //    get 
        //    { 
        //        return new Rectangle((int)Position.X, (int)Position.Y, (int)(HalfSize.X * 2 * Scale.X), (int)(HalfSize.Y * 2 * Scale.Y)); 
        //    }
        //}

        //public Vector2 HalfSize
        //{
        //    get;
        //    private set;
        //}

        public Vector2 Origin
        {
            get;
            private set;
        }

        public Transform(GameObject gObj)
        {
            gameObject = gObj;
            _destRect = new Rectangle();
            Initialize(Vector2.Zero, 0, Vector2.One);
        }

        public void Initialize(Vector2 position, float rotation, Vector2 scale)
        {
            LocalPosition = position;
            Rotation = rotation;
            Scale = scale;

            // TODO: Remove Origin?
            // halfsize == Origin;
            //Origin = halfsize;
        }
    }
}
