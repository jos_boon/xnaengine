using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace XNAEngine.ScreenCollection
{
    // TODO: Link to PhysicsWorld
    public class Screen : DrawableGameComponent
    {
        public GameComponentCollection GameObjects { get; private set; }
        public bool Overlay { get; protected set; }

        public SpriteSortMode   spriteSortMode;
        public RasterizerState  rasterState;
        public BlendState       blendState;

        public bool Initialized { get; private set; }

        public Screen(Game game) : base(game)
        {
            GameObjects = new GameComponentCollection();

            spriteSortMode = SpriteSortMode.Deferred;
            blendState = BlendState.AlphaBlend;
            rasterState = null;
            Overlay = false;

            GameObjects.ComponentAdded += OnGameComponentAdded;
            GameObjects.ComponentRemoved += OnGameComponentRemoved;
        }

        public override void Initialize()
        {
            foreach (IGameComponent component in GameObjects)
                component.Initialize();

            Initialized = true;

            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            // Credits to: Nils Dijk & Koen Bollen
            // https://gist.github.com/koenbollen/925477
            foreach(IUpdateable gUpdate in GameObjects.OfType<IUpdateable>().Where<IUpdateable>(x=>x.Enabled).OrderBy<IUpdateable, int>(x=>x.UpdateOrder))
                gUpdate.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // Credits to: Nils Dijk & Koen Bollen
            // https://gist.github.com/koenbollen/925477
            foreach (IDrawable gDraw in GameObjects.OfType<IDrawable>().Where<IDrawable>(x=>x.Visible).OrderBy<IDrawable, int>(x=>x.DrawOrder))
                gDraw.Draw(gameTime);
        }

        protected override void Dispose(bool disposing)
        {
            foreach (GameObject gObj in GameObjects)
                gObj.Dispose();

            this.GameObjects.Clear();

            base.Dispose(disposing);
        }

        public void Empty()
        {
            this.Dispose();   
        }

        protected virtual void OnGameComponentAdded(object sender, GameComponentCollectionEventArgs component)
        {

        }

        protected virtual void OnGameComponentRemoved(object sender, GameComponentCollectionEventArgs component)
        {

        }

        public void ExitScreen()
        {
            ScreenManager.Instance.RemoveScreen(this);
        }
    }
}
