using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace XNAEngine.ScreenCollection
{
    // TODO: Add Debug screen behind the scenes (new DebugScreen(Game))
    // TODO: private constructor (singleton)?
    public class ScreenManager : DrawableGameComponent
    {
        // TODO: find Singleton solution for xna.
        private static ScreenManager    _instance;
        private SpriteBatch             spriteBatch;
        private List<Screen>            _screens;
        public Color                    ClearColor;

        public static ScreenManager Instance
        {
            get
            {
                return _instance;
            }
        }

        /// <summary>
        /// Get a copy of the List screen
        /// </summary>
        public List<Screen> Screens { get { return _screens.ToList(); } }

        public bool Initialized { get; private set; }

        public ScreenManager(Game game)
            : base(game)
        {
            _screens = new List<Screen>();
            spriteBatch = Game.Services.GetService(typeof(SpriteBatch)) as SpriteBatch;
            _instance = this;
            ClearColor = Color.CornflowerBlue;
        }

        // TODO: Init a debug screen for us? (Set Debugscreen to interal)
        public override void Initialize()
        {
            foreach (Screen s in _screens)
                s.Initialize();

            Initialized = true;

            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            // TODO: Screen switching and adding durning an update will result an error (can't update a list durning an enumuration)
            foreach (Screen s in Screens.Where<IUpdateable>(x => x.Enabled))
                s.Update(gameTime);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(ClearColor);
            foreach (Screen s in _screens.Where<IDrawable>(x => x.Visible))
            {
                spriteBatch.Begin(s.spriteSortMode, s.blendState, null, null, s.rasterState);
                    s.Draw(gameTime);
                spriteBatch.End();
            }

            base.Draw(gameTime);
        }

        public void LoadInScreen(params Screen[] LoadInScreens)
        {
            if (LoadInScreens.Length > 0)
            {
                foreach (Screen s in Screens)
                    s.ExitScreen();

                foreach (Screen s in LoadInScreens)
                {
                    if (s != null)
                        AddScreen(s);
                }
            }
        }

        public T[] GetScreen<T>()
        {
            return _screens.OfType<T>().ToArray<T>();
        }

        public void AddScreen(Screen s)
        {
            if (!s.Initialized && Initialized)
                s.Initialize();

            _screens.Add(s);
        }

        public void RemoveScreen(Screen s)
        {
            _screens.Remove(s);
            s.Dispose();
        }
    }
}
