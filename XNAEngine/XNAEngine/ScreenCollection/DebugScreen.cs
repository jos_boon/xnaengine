﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XNAEngine.InputHandlers;
using XNAEngine.Physics;
using XNAEngine.XNAConsole;

namespace XNAEngine.ScreenCollection
{
    public class DebugScreen : Screen
    {
        public bool ShowBorders = false;
        public bool ShowVectors = false;
        public bool ShowColliders = false;
        public bool ShowConsole = false;
        public bool ShowFPS = true;

        private SpriteBatch _batch;
        private FPSCounter  _fps;
        private GameConsole _con;

        public DebugScreen(Game game) : base(game)
        {
            _batch = (SpriteBatch)game.Services.GetService(typeof(SpriteBatch));
            _fps = new FPSCounter();
            _fps.Position = new Vector2(5, 5);
        }

        public override void Initialize()
        {
            Overlay = true;
            spriteSortMode = SpriteSortMode.Immediate;
            blendState = BlendState.AlphaBlend;
            rasterState = new RasterizerState() { ScissorTestEnable = true };

            InitConsole();

            base.Initialize();
        }

        private void InitConsole()
        {
            PhysicsWorld world = Game.Services.GetService(typeof(PhysicsWorld)) as PhysicsWorld;

            _con = GameConsole.BuildConsole(Game, world, this, new Vector2(600, 400));
            _con.transform.LocalPosition = new Vector2(720, 450);

            Game.Services.AddService(typeof(GameConsole), _con);

            Command c = new Command();
            c.name = "TOGGLE";
            c.argsName = "<BORDERS|VECTORS|COLLIDERS|FPS>";
            c.description = "Toggle debug information";
            c.execute = ToggleCommand;

            _con.Commands.Add(c);

            _con.Toggle();
        }

        private void ToggleCommand(GameConsole sender, string[] args)
        {
            if (args.Length > 1)
            {
                if (args[1] == "BORDERS")
                    ShowBorders = !ShowBorders;
                else if (args[1] == "VECTORS")
                    ShowVectors = !ShowVectors;
                else if (args[1] == "COLLIDERS")
                    ShowColliders = !ShowColliders;
                else if (args[1] == "FPS")
                    ShowFPS = !ShowFPS;
                else
                    sender.WriteLine(String.Format("ERROR: Unknown TOGGLE command: {0}!", args[1]));
            }
            else
                sender.WriteLine("ERROR: Toggle command missing argument!");
        }

        protected override void Dispose(bool disposing)
        {
            Game.Services.RemoveService(typeof(GameConsole));
            base.Dispose(disposing);
        }

        public override void Update(GameTime gameTime)
        {
            DebugInput();

            if (ShowFPS)
                _fps.Update(gameTime);

            base.Update(gameTime);
        }
        
        private void DebugInput()
        {
            // Toggle Console
            if (InputManager.keyboardHandler.getExtendedKeyState(Keys.OemTilde) == ExtendButtonState.JustReleased)
                _con.Toggle();
        }

        public override void Draw(GameTime gameTime)
        {
            if(ShowFPS)
                _fps.Draw(gameTime);

            // No reason to draw or loop through the list if it is empty or the commands are disabled
            if (ShowBorders || ShowVectors || ShowColliders)
            {
                DrawPrimitives.DrawingLayer = LayerDepthOrder.DebugLayer;

                foreach (Screen s in ScreenManager.Instance.Screens)
                {
                    // TODO: figure out if oftype can be left out
                    foreach (GameObject gObj in s.GameObjects.OfType<GameObject>().Where<GameObject>(x => x.Visible))
                    {
                        if (ShowBorders)
                        {
                            // Rectangle
                            if(gObj.sprite != null)
                                DrawPrimitives.DrawRectangle(gObj.transform.Position, gObj.sprite.Halfsize, gObj.transform.Scale, gObj.transform.Rotation, null, Color.Red);
                        }

                        if (ShowVectors && gObj.rigidbody != null)
                        {
                            //Direction Vectors
                            //DrawPrimitives.DrawLine(gObj.transform.Position, gObj.transform.Position + gObj.transform.Right * 40, Color.Yellow);
                            //DrawPrimitives.DrawLine(gObj.transform.Position, gObj.transform.Position + gObj.transform.Up * 40, Color.Violet);
                            DrawPrimitives.DrawLine(gObj.transform.Position, gObj.transform.Position + gObj.rigidbody.Velocity * 40, Color.Red);
                        }

                        if (ShowColliders && gObj.collider != null)
                        {
                            AABBCollider box = (AABBCollider)gObj.collider;
                            DrawPrimitives.DrawLine(new Vector2[4] { box.TopLeftCorner, box.TopRightCorner, box.DownRightCorner, box.DownLeftCorner }, gObj.transform.Position, Color.LightGreen, 1, true);
                        }
                    }
                }  
            }

            base.Draw(gameTime);
        }     
    }
}
