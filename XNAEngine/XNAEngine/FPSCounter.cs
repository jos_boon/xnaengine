﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XNAEngine
{
    class FPSCounter
    {
        private int     _total_frames = 0;
        private float   _elapsed_time = 0.0f;
        private int     _fps = 0;

        public Vector2 Position { get; set; }

        public void Update(GameTime gameTime)
        {
            _elapsed_time += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if(_elapsed_time >= 1000.0f)
            {
                _fps = _total_frames;
                _total_frames = 0;
                _elapsed_time -= 1000.0f;
            }
        }

        public void Draw(GameTime gameTime)
        {
            _total_frames++;

            DrawPrimitives.DrawText(string.Format("FPS:{0}", _fps), Position, Color.White);
        }
    }
}
