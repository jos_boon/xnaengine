﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using Microsoft.Xna.Framework.Content;

namespace XNAEngine
{
    // TODO: Improve draw functions
    // TODO: Look into Drawing Primitives functions: https://msdn.microsoft.com/en-us/library/bb196414.aspx, http://xbox.create.msdn.com/en-US/education/catalog/sample/primitives
    // TODO: Getter for default font object

    public static class DrawPrimitives
    {
        private static Texture2D        _texture;
        private static SpriteFont       _font; // TODO: FONT is not supporting all symbols
        private static SpriteBatch      _batch;
        private static GraphicsDevice   _device;
        public static LayerDepthOrder DrawingLayer
        {
            get;
            set;
        }

        public static void Initialize(Game game, GraphicsDevice device)
        {
            _batch = (SpriteBatch)game.Services.GetService(typeof(SpriteBatch));
            _device = device;

            _texture = new Texture2D(device, 1, 1);
            _texture.SetData<Color>(new Color[]{Color.White});
            
            _font = XNAEngineContentLib.Content.Load<SpriteFont>("defaultFont");

            DrawingLayer = LayerDepthOrder.DefaultLayer;
        }

        /// <summary>
        /// Offset the array of vertices to the first quadrant of the coordinate system
        /// </summary>
        /// <param name="vertices">Array of vertices that needs to be offset to the first quadrant</param>
        /// <param name="size">An out value that will give you the size needed fit the vertices in a rectangle</param>
        /// <returns>Array of vertices that have been offset to fit within the first quadrant</returns>
        private static Vector2[] VerticesOffsetToFirstQuadrant(Vector2[] vertices, out Vector2 size)
        {
            Vector2 offset = vertices[0];
            size = new Vector2();
            size.X = 1;
            size.Y = 1;
            
            for (int i = 0; i < vertices.Length; i++)
            {
                // set offset
                if (vertices[i].X < offset.X)
                    offset.X = vertices[i].X;

                if (vertices[i].Y < offset.Y)
                    offset.Y = vertices[i].Y;

                // Texture size
                if (vertices[i].X > size.X)
                    size.X = vertices[i].X;

                if (vertices[i].Y > size.Y)
                    size.Y = vertices[i].Y;
            }

            // Shift the vertices so it fits within the texture we are about to create
            for (int i = 0; i < vertices.Length; i++)
                vertices[i] -= offset;

            // Adjust size base on offset
            size -= offset;

            return vertices;
        }

        /// <summary>
        /// Create a Quadrilateral (Sqaure, Rectangle, Rhombus, Parallelogram, Trapezoid, Kite or other 4 sided shapes)
        /// </summary>
        /// <returns>Texture of the newly created quadrilateral</returns>
        public static Texture2D CreateQuadrilateral(Vector2[] vertices, Nullable<Color> fill, Nullable<Color> border = null, int borderWidth = 1)
        {
            Vector2 size = new Vector2();

            vertices = VerticesOffsetToFirstQuadrant(vertices, out size);

            // Create Quadrilateral
            Texture2D texQuad = new Texture2D(_device, (int)size.X, (int)size.Y);
            int colorSize = (int)(size.X * size.Y);
            Color[] colors = new Color[colorSize];

            // TODO: Alogorithm needed for fill pixels
            // Set pixel colors
            if(fill.HasValue)
            {
                for (int i = 0; i < colorSize; i++)
                    colors[i] = fill.Value;
            }
            
            if(border.HasValue)
                AddLines(colors, size, vertices, border.Value, borderWidth, true);

            // TODO: Size of non-integers will cause an exception (size of array to large or to small)
            texQuad.SetData<Color>(colors);

            return texQuad;
        }

        public static Texture2D CreateCircle(int radius, Nullable<Color> fill)
        {
            Texture2D texCircle = new Texture2D(_device, 2 * radius, 2 * radius);
            int quarterSize = radius * radius;
            Vector2 center = new Vector2(radius, radius);
            Color[] colors = new Color[4 * quarterSize];

            // Set pixel colors
            for (int i = 0; i < quarterSize; i++)
            {
                int x = i % (texCircle.Width / 2);
                int y = (i - x) / (texCircle.Width / 2);
                Vector2 point = new Vector2(x, y);

                // Only color the pixel within the radius
                float distance = Vector2.Distance(center, point);
                if (distance <= radius)
                {
                    // Translate the x & y coordinates to the WxH dimensions of the color array
                    int p = x + y * texCircle.Width;

                    // Top Left quarter
                    colors[p] = fill.Value;

                    // Top Right Quarter
                    colors[y * texCircle.Width + texCircle.Width - x - 1] = fill.Value;
                    
                    // Bottom Left Quarter
                    colors[x + (texCircle.Height - y - 1) * texCircle.Width] = fill.Value;

                    // Bottom Right Quarter
                    colors[texCircle.Width * texCircle.Height - p - 1] = fill.Value;
                }    
            }

            texCircle.SetData<Color>(colors);

            return texCircle;
        }

        //TODO: Add Lines is copy pasta from drawLines function
        private static void AddLines(Color[] textureColors, Vector2 size, Vector2[] vertices, Color lineColor, int lineThickness = 1, bool completeLine = true)
        {
            if(completeLine)
            {
                for (int i = 0; i < vertices.Length; i++)
                    CreateLine(textureColors, size, vertices[i], vertices[(i + 1) % vertices.Length], lineColor, lineThickness);
            }
            else
            {
                for (int i = 0; i < vertices.Length - 1; i++)
                    CreateLine(textureColors, size, vertices[i], vertices[i + 1], lineColor, lineThickness); 
            }
            
        }

        // TODO: add LineThickness code
        private static void CreateLine(Color[] textureColors, Vector2 size, Vector2 start, Vector2 end, Color c, int lineThickness)
        {
            // Per pixel step
            Vector2 delta = end - start;
            // Amount of pixels we need to fill
            int length = (int)delta.Length();
            
            // Make Length one
            delta.Normalize();

            // X and y are used as indexes
            float x = start.X >= size.X ? size.X - 1 : start.X;
            float y = start.Y >= size.Y ? size.Y - 1 : start.Y;

            // Create line
            for (int i = 0; i < length; i++)
            {
                int pixel = (int)(x + (int)(y) * size.X);
                textureColors[pixel] = c;

                x += delta.X;
                y += delta.Y;
            }
        }



        public static void DrawLine(Vector2 start, Vector2 end, Color c, int lineWidth = 1)
        {
            Vector2 delta = end - start;
            float angle = (float)Math.Atan2(delta.Y, delta.X);
            
            // Draw Line
            _batch.Draw(_texture, start, null, c, angle, new Vector2(0, (lineWidth == 1) ? 0 : .5f), new Vector2(delta.Length(), lineWidth), SpriteEffects.None, DrawingLayer.Value);
        }

        public static void DrawLine(Vector2[] vertices, Vector2 offset, Color c, int LineWidth = 1, bool connect = false)
        {
            if(connect)
            {
                for (int i = 0; i < vertices.Length; i++)
                {
                    DrawLine(offset + vertices[i], offset + vertices[(i + 1) % vertices.Length], c, LineWidth);
                }
            }
            else
            {
                for (int i = 0; i < vertices.Length - 1; i++)
                {
                    DrawLine(offset + vertices[i], offset + vertices[i + 1], c, LineWidth);
                }
            }
            
        }

        public static void DrawRectangle(Vector2 position, Vector2 halfsize, Vector2 scale, float rotation, Nullable<Color> fill, Nullable<Color> border = null, int borderWidth = 1)
        {
            if (fill.HasValue)
            {
                _batch.Draw(_texture, position, null, fill.Value, MathHelper.ToRadians(rotation), new Vector2(0.5f, 0.5f), halfsize * 2 * scale, SpriteEffects.None, DrawingLayer.Value);
            }

            if(border.HasValue)
            {
                Vector2[] vertices = new Vector2[4] 
                { 
                    new Vector2(-halfsize.X, -halfsize.Y) * scale,  // Top Left
                    new Vector2(halfsize.X, -halfsize.Y) * scale,   // Top Right
                    new Vector2(halfsize.X, halfsize.Y) * scale,    // Bottom Right
                    new Vector2(-halfsize.X, halfsize.Y) * scale    // Bottom Left    
                };

                for (int i = 0; i < 4; i++)
                    vertices[i] = Vector2.Transform(vertices[i], Matrix.CreateRotationZ(MathHelper.ToRadians(rotation)));

                DrawLine(vertices, position, border.Value, borderWidth, true);
            }
        }

        public static void DrawText(String text, Vector2 position, Color c)
        {
            _batch.DrawString(_font, text, position, c, 0, Vector2.Zero, 1, SpriteEffects.None, DrawingLayer.Value);
        }
     
    }
}
