﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace XNAEngine
{
    // TODO: Expand this class or remake to serve as a general Init for Engine components. (examples: DrawPrimitives, Physics and InputHandlers)
    // Info for ContentManager: https://msdn.microsoft.com/en-us/library/ff604975.aspx
    public class XNAEngineContentLib //: DrawableGameComponent
    {
        private static ContentManager content;

        public static ContentManager Content
        {
            get 
            {
                if (content != null)
                    return content;
                else
                    throw new NullReferenceException("Content variable is returning null. Call XNAEngineContentLib.Initialize(Game) before accessing this property");
            }
        }

        //public XNAEngineContentLib (Game game) : base(game)
        //{      
        //    content = new ContentManager(game.Services);
        //    content.RootDirectory = "XNAEngineContent";
        //}

        public static void Initialize(Game game)
        {
            content = new ContentManager(game.Services);
            content.RootDirectory = "XNAEngineContent";
        }


        /// <summary>
        /// Rotate the vector x and y component around the given point in n-radius direction
        /// </summary>
        /// <param name="v">Vector you want to rotate</param>
        /// <param name="degree">Amount of rotation in degrees</param>
        /// <param name="rotatePoint">Point you want to rotate around it</param>
        /// <returns>Returns a new vector thas has been rotated around the given rotation point</returns>
        public static Vector2 RotateVector(Vector2 v, float degree, Vector2 rotatePoint)
        {
            Vector2 nVector = Vector2.Zero;
            float radians = MathHelper.ToRadians(degree);

            // Translate vector
            v -= rotatePoint;

            // Rotate Vector
            nVector.X = (float)(v.X * Math.Cos(radians) - v.Y * Math.Sin(radians));
            nVector.Y = (float)(v.X * Math.Sin(radians) + v.Y * Math.Cos(radians));

            // Translate Vector back to new position
            nVector += rotatePoint;

            return nVector;
        }
    }
}
