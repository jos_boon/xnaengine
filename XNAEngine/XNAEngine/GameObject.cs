using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Graphics;

using XNAEngine.Physics;
using XNAEngine.InputHandlers;
using XNAEngine.ScreenCollection;


namespace XNAEngine
{
    // TODO: Change on enable should affect all components
    // TODO: transform, collider, rigidbody, ect should be part of a component base class
    // TODO: these components need an enable property
    // TODO: Call Initialize of components when added
    // TODO: Make it a component based system

    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GameObject : DrawableGameComponent
    {
        protected PhysicsWorld      _world;
        protected SpriteBatch       _batch;
        
        private GameObject          _parent;
        private Collider            _collider;

        // TODO: add child won't set child parent relation
        public List<GameObject>     Children;
        public String               Tag;

        //TODO: make transform readonly everywhere
        public readonly Transform   transform;
        public Sprite               sprite;
        public Rigidbody            rigidbody;
        
        public Collider             collider
        {
            get
            {
                return _collider;
            }
            set
            {
                // Remove old collider
                if(_collider != null)
                    _collider.Dispose();

                _collider = value;
                _world.AddCollider(_collider);
            }
        }

        /// <summary>
        /// Make the GameObject child of parent (set it to null to remove parent)
        /// </summary>
        public GameObject           Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                if (_parent != null)
                    _parent.Children.Remove(this);

                _parent = value;

                if (_parent != null)
                    _parent.Children.Add(this);

                //TODO: When Parent is set to null LocalPosition isn't updated (intentional?)
            }
        }

        public GameObject(Game game, PhysicsWorld world)
            : base(game)
        {
            _batch = (SpriteBatch)game.Services.GetService(typeof(SpriteBatch));
            Children = new List<GameObject>();
            transform = new Transform(this);
            _world = world;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: before Init the old rigidbody and colliders needs to be removed from the world
            // rigidbody = new Rigidbody();
            // collider = new Collider();
            
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // Draw sprite if component is added
            if (sprite != null)
                sprite.Draw(gameTime);

            base.Draw(gameTime);
        }

        /// <summary>
        /// Clean up the Objects that still have a reference after this object doesn't exist anymore
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            // Clean up rigidbody & collider from physics world
            if(rigidbody != null)
                rigidbody.Dispose();

            if(collider != null)
                collider.Dispose();

            // TODO: break Child Parent link OR dispose of child list

            base.Dispose(disposing);
        }
    }
}
