﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAEngine.Observer
{
    public interface ISubject
    {
        void Attach(ICustomObserver observer);
        void Detach(ICustomObserver observer);
        void Notify();
    }
}
