﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XNAEngine.Observer
{
    public interface ICustomObserver
    {
        void OnNotify(ISubject sender);
    }
}
