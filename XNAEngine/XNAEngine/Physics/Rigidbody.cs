﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using XNAEngine;

namespace XNAEngine.Physics
{
    public class Rigidbody : IDisposable
    {
        private PhysicsWorld    _world;
        private Transform       _transform;
        private GameObject      _gameObject; // reference back to the game object

        private Vector2         _force;
        private bool            _enabled;

        public bool IsKinematic = false;

        public Vector2         Velocity { get; set; }
        public float           Mass { get; set; }
        public float           Friction { get; set; }

        public RigidbodyType type { get; set; }

        public Rigidbody(GameObject gameObj, PhysicsWorld world)
        {
            _world = world;
            _gameObject = gameObj;
            _transform = gameObj.transform;
            type = RigidbodyType.Dynamic;

            Mass = 10;
            Friction = .2f;
            _enabled = true;

            _world.AddRigidbody(this);
        }

        public bool Enabled
        {
            get { return _enabled; }
            set { _enabled = value; }
        }

        public void Update(GameTime gameTime)
        {
            // Update rigidbody
            Velocity += _force * (float)gameTime.ElapsedGameTime.TotalSeconds / Mass;

            _force *= Friction;

            if (_force.Length() <= 0.1f)
                _force = Vector2.Zero;

            _transform.LocalPosition += Velocity;
        }

        public void addForce(Vector2 force)
        {
            this._force += force;
        }
    
        public void  Dispose()
        {
            if (_world != null) 
                _world.DisposeObject(this);
        }

        public enum RigidbodyType
        {
            Dynamic,
            Kinematic
        }
    }
}
