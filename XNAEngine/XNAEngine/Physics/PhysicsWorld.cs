﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using XNAEngine;
using XNAEngine.InputHandlers;

namespace XNAEngine.Physics
{
    /// <summary>
    /// Physics World for 2D games.
    /// Updates rigidbodies, detects and handles collision
    /// </summary>
    public class PhysicsWorld : GameComponent
    {
        private List<Rigidbody>     _rigidbodies;
        private List<Collider>      _colliders;
        public Vector2              Gravity = new Vector2(0, 9.81f);

        public PhysicsWorld(Game game)
            : base(game)
        {
            _rigidbodies = new List<Rigidbody>();
            _colliders = new List<Collider>();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (Rigidbody body in _rigidbodies.Where<Rigidbody>(x=>x.Enabled))
                body.Update(gameTime);

            CollisionDetection();

            base.Update(gameTime);
        }

        private void ApplyPhysics(GameTime gameTime)
        {
            throw new NotImplementedException("ApplyPhysics: We're missing code here!");
        }

        private void CollisionDetection()
        {
            var colliders = _colliders.Where<Collider>(x => x.Enabled).ToList<Collider>();
            foreach (Collider c in colliders)
            {
                // Check if mouse is within the collider object
                c.MouseDetection();

                foreach (Collider other in colliders)
                {
                    // No Collision detection IF:
                    // 1. Both variables are refering to the same object
                    // 2. They are static objects (no rigidbodies)
                    if (c == other || (c.rigidbody == null && other.rigidbody == null))
                        continue;

                    c.HandleCollision(other);
                }
            }
        }

        public void AddRigidbody(Rigidbody b)
        {
            _rigidbodies.Add(b);
        }

        public void AddCollider(Collider c)
        {
            _colliders.Add(c);
        }

        public void DisposeObject(IDisposable obj)
        {
            if (obj is Rigidbody)
                _rigidbodies.Remove(obj as Rigidbody);
            else if (obj is Collider)
                _colliders.Remove(obj as Collider);
            else
                Console.WriteLine("Unknown Object sent to remove from Physics World! Object of type {0} not supported!", obj.GetType()); // TODO: Use game console?
        }

        public void numbers()
        {
            Console.WriteLine(_rigidbodies.Count + "\n" + _colliders.Count); 
        }
    }
}
