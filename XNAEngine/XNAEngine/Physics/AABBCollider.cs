using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using XNAEngine;


namespace XNAEngine.Physics
{
    // TODO: Without a gameobject transform it breaks apart
    public class AABBCollider : Collider
    {
        // TODO: Corners are local position (intended?)
        public Vector2 TopLeftCorner
        {
            get;
            private set;
        }

        public Vector2 TopRightCorner
        {
            get;
            private set;
        }

        public Vector2 DownLeftCorner
        {
            get;
            private set;
        }

        public Vector2 DownRightCorner
        {
            get;
            private set;
        }

        public AABBCollider(Vector2 halfsize, GameObject gameObj, PhysicsWorld world)
            : base(gameObj, world)
        {
            if (_transform != null && _transform.Rotation != 0)
                ReInitialize();
            else
                SetCorners(halfsize);

            Halfsize = halfsize;
        }

        public override void ReInitialize()
        {
            SetCorners(Halfsize); // Half size of transform doesn't rotate

            if (_transform != null && _transform.Rotation % 360 != 0)
            {
                TopLeftCorner = RotateCorner(TopLeftCorner, _transform.Rotation);
                TopRightCorner = RotateCorner(TopRightCorner, _transform.Rotation);
                DownLeftCorner = RotateCorner(DownLeftCorner, _transform.Rotation);
                DownRightCorner = RotateCorner(DownRightCorner, _transform.Rotation);

                Vector2 topLeftDownRight = TopLeftCorner - DownRightCorner;
                Vector2 downLeftTopRight = DownLeftCorner - TopRightCorner;

                topLeftDownRight.X = Math.Abs(topLeftDownRight.X);
                topLeftDownRight.Y = Math.Abs(topLeftDownRight.Y);
                downLeftTopRight.X = Math.Abs(downLeftTopRight.X);
                downLeftTopRight.Y = Math.Abs(downLeftTopRight.Y);

                float X = MathHelper.Max(topLeftDownRight.X, downLeftTopRight.X);
                float Y = MathHelper.Max(topLeftDownRight.Y, downLeftTopRight.Y);

                Vector2 halfsize = new Vector2(X, Y) / 2;

                SetCorners(halfsize);
            }

            base.ReInitialize();
        }

        private void SetCorners(Vector2 halfsize)
        {
            TopLeftCorner = -halfsize;
            TopRightCorner = new Vector2(halfsize.X, -halfsize.Y);
            DownLeftCorner = new Vector2(-halfsize.X, halfsize.Y);
            DownRightCorner = halfsize;
        }

        private Vector2 RotateCorner(Vector2 cor, float rot)
        {
            float rad = MathHelper.ToRadians(rot);
            return Vector2.Transform(cor, Matrix.CreateRotationZ(rad));
        }
    }
}
