﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using XNAEngine;
using XNAEngine.InputHandlers;

namespace XNAEngine.Physics
{
    public delegate void CollisionHandler(Collider other);
    abstract public class Collider : IDisposable
    {
        // TODO: Collider groups (use enum flags for bitwise operation)
        // TODO: Rewrite the Handle Collision
        // TODO: add support for different collider types
        private event CollisionHandler  OnCollisionEnter;
        private event CollisionHandler  OnCollisionExit; 
        private event CollisionHandler  OnTriggerEnter;
        private event CollisionHandler  OnTriggerExit;
        private event CollisionHandler  OnCollisionStay;
        private event CollisionHandler  OnTriggerStay;

        private event EventHandler      OnMouseClick;
        private event EventHandler      OnMouseDoubleClick;
        private event EventHandler      OnMouseHold;
        private event EventHandler      OnMouseHover;
        private event EventHandler      OnMouseExit;

        public bool IsMouseDetected { get; private set; }

        public readonly Transform       _transform;
        private Rigidbody               _rigidbody;
        private PhysicsWorld            _world;
        private GameObject              _gameObject;
        private List<Collider>          _prevCollided;
        private Vector2                 _halfsize;

        // Trigger means it only checks if there is collision but it will not preform any physics handling afterwards (Like how Unity does it)
        public bool                     TriggerCollider;

        public Vector2                  Halfsize
        {
            get { return _halfsize; }
            set 
            {
                _halfsize = value;
                ReInitialize();
            }
        }

        public GameObject               gameObject
        {
            get
            {
                return _gameObject;
            }
        }

        public Rigidbody                rigidbody
        {
            get
            {
                return _rigidbody;
            }
            set
            {
                _rigidbody = value; 
            }
        }

        public bool                     Enabled { get; set; }

        public Collider(GameObject gameObj, PhysicsWorld world)
        {
            IsMouseDetected = false;

            // Link the transform, gameobject and collider
            if(gameObj != null)
            {
                _gameObject = gameObj;
                _gameObject.collider = this;
                _rigidbody = _gameObject.rigidbody;
                _transform = gameObj.transform;
            }

            if(world != null)
                _world = world;

            _prevCollided = new List<Collider>();
            Enabled = true;
        }

        internal void HandleCollision(Collider other)
        {
            CollisionHandler handler = null;
            bool overlapDetected = false;

            Type typeCol = this.GetType();
            Type oTypeCol = other.GetType();

            // Check if c is overlapping other through AABB detection
            if (typeCol == typeof(AABBCollider) && oTypeCol == typeof(AABBCollider))
                overlapDetected = Intersection.AABBonAABB(this as AABBCollider, other as AABBCollider);
            else if (typeCol == typeof(CircleCollider) && oTypeCol == typeof(CircleCollider))
                overlapDetected = Intersection.CircleOnCircle(this as CircleCollider, other as CircleCollider);
            else if (typeCol == typeof(AABBCollider) && oTypeCol == typeof(CircleCollider))
                overlapDetected = Intersection.AABBonCircle3(this as AABBCollider, other as CircleCollider);
            else if (typeCol == typeof(CircleCollider) && oTypeCol == typeof(AABBCollider)) // <-- Swapping box and circle around
                overlapDetected = Intersection.AABBonCircle3(other as AABBCollider, this as CircleCollider);

            if (overlapDetected)
            {
                // IF Object doesn't exist THEN CALL OnCollision || OnTrigger
                if (!_prevCollided.Exists(x => x == other))
                {      
                    _prevCollided.Add(other);

                    if (!this.TriggerCollider)
                        handler = this.OnCollisionEnter;
                    else
                        handler = this.OnTriggerEnter; 
                }
                else // ELSE CALL OnCollisionStay || OnTriggerStay
                {
                    if (!this.TriggerCollider)
                        handler = this.OnCollisionStay;
                    else
                        handler = this.OnTriggerStay;
                }

                // Resolve Collision
                if(!this.TriggerCollider && !other.TriggerCollider)
                {

                }
            }
            else // ON EXIT Collision
            {
                if (_prevCollided.Exists(x => x == other))
                {
                    _prevCollided.Remove(other);

                    if (!this.TriggerCollider)
                        handler = this.OnCollisionExit;
                    else
                        handler = this.OnTriggerExit;
                }        
            }

            if(handler != null)
            {
                foreach (Delegate d in handler.GetInvocationList())
                    d.DynamicInvoke(other);
            }
        }

        // TODO: Does mouse detection work on all kinds of colliders?
        /// <summary>
        /// Detects and handles the mouse events within this collider
        /// </summary>
        internal void MouseDetection()
        {
            EventHandler handler = null;
            MouseHandler mouse = InputManager.mouseHandler;
            
            // Check if current mouse is still inside the collider
            bool cMouseDetection = false;
            if (this.GetType() == typeof(AABBCollider))
                cMouseDetection = Intersection.PointOnAABB(this as AABBCollider, mouse.MousePosition);
            else if (this.GetType() == typeof(CircleCollider))
                cMouseDetection = Intersection.PointOnCircle(this as CircleCollider, mouse.MousePosition);

            if (cMouseDetection)
            {
                IsMouseDetected = true;

                if (mouse.ExtendLeftButton == ExtendButtonState.JustReleased)
                    handler = OnMouseClick;
                else if (mouse.ExtendLeftButton == ExtendButtonState.Holding)
                    handler = OnMouseHold;
                else
                    handler = OnMouseHover;

            } // If mouse pointer was recently detected then fire exit event handler
            else if (IsMouseDetected)
            {
                IsMouseDetected = false;
                handler = OnMouseExit;
            }

            if(handler != null)
            {
                foreach (Delegate d in handler.GetInvocationList())
                {
                    d.DynamicInvoke(this, new EventArgs());
                }
            }
        }

        public virtual void ReInitialize() 
        {

        }

        public void AttachCollisionEvent(CollisionHandler col, EventType type)
        {
            switch (type)
            {
                case EventType.OnCollisionEnter:
                    OnCollisionEnter += col;
                    break;
                case EventType.OnCollisionStay:
                    OnCollisionStay += col;
                    break;
                case EventType.OnCollisionExit:
                    OnCollisionExit += col;
                    break;
                case EventType.OnTriggerEnter:
                    OnTriggerEnter += col;
                    break;
                case EventType.OnTriggerStay:
                    OnTriggerStay += col;
                    break;
                case EventType.OnTriggerExit:
                    OnTriggerExit += col;
                    break;
            }            
        }

        public void AttachMouseEvent(EventHandler eventHandler, MouseEventType type)
        {
            switch (type)
            {
                case MouseEventType.OnMouseClick:
                    OnMouseClick += eventHandler;
                    break;
                case MouseEventType.OnMouseDoubleClick:
                    OnMouseDoubleClick += eventHandler;
                    break;
                case MouseEventType.OnMouseHold:
                    OnMouseHold += eventHandler;
                    break;
                case MouseEventType.OnMouseHover:
                    OnMouseHover += eventHandler;
                    break;
                case MouseEventType.OnMouseExit:
                    OnMouseExit += eventHandler;
                    break;
            }
        }

        public void DetachCollisionEvent(CollisionHandler col, EventType type)
        {
            switch (type)
            {
                case EventType.OnCollisionEnter:
                    OnCollisionEnter -= col;
                    break;
                case EventType.OnCollisionStay:
                    OnCollisionStay -= col;
                    break;
                case EventType.OnCollisionExit:
                    OnCollisionExit -= col;
                    break;
                case EventType.OnTriggerEnter:
                    OnTriggerEnter -= col;
                    break;
                case EventType.OnTriggerStay:
                    OnTriggerStay -= col;
                    break;
                case EventType.OnTriggerExit:
                    OnTriggerExit -= col;
                    break;
            }           
        }

        public void DetachMouseEvent(EventHandler eventHandler, MouseEventType type)
        {
            switch (type)
            {
                case MouseEventType.OnMouseClick:
                    OnMouseClick -= eventHandler;
                    break;
                case MouseEventType.OnMouseDoubleClick:
                    OnMouseDoubleClick -= eventHandler;
                    break;
                case MouseEventType.OnMouseHold:
                    OnMouseHold -= eventHandler;
                    break;
                case MouseEventType.OnMouseHover:
                    OnMouseHover -= eventHandler;
                    break;
                case MouseEventType.OnMouseExit:
                    OnMouseExit -= eventHandler;
                    break;
            }
        }

        public void Dispose()
        {
            if(_world != null)
                _world.DisposeObject(this);
        }

        public enum EventType
        {
            OnCollisionEnter,
            OnCollisionStay,
            OnCollisionExit,
            OnTriggerEnter,
            OnTriggerStay,
            OnTriggerExit
        }

        public enum MouseEventType
        {
            OnMouseClick,
            OnMouseDoubleClick,
            OnMouseHold,
            OnMouseHover,
            OnMouseExit
        }
    }
}
