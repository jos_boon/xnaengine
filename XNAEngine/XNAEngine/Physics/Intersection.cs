﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XNAEngine.Physics
{
    static class Intersection
    {
        public static bool AABBonAABB(AABBCollider box, AABBCollider otherBox)
        {
            Vector2 boxMin = box._transform.Position + box.TopLeftCorner;
            Vector2 boxMax = box._transform.Position + box.DownRightCorner;
            Vector2 otherBoxMin = otherBox._transform.Position + otherBox.TopLeftCorner;
            Vector2 otherBoxMax = otherBox._transform.Position + otherBox.DownRightCorner;

            return (boxMin.X <= otherBoxMax.X && boxMax.X >= otherBoxMin.X)
                && (boxMin.Y <= otherBoxMax.Y && boxMax.Y >= otherBoxMin.Y);
        }

        public static bool CircleOnCircle(CircleCollider circle, CircleCollider otherCircle)
        {
            Vector2 delta = circle._transform.Position - otherCircle._transform.Position;

            return delta.Length() <= (circle.Radius + otherCircle.Radius);
        }

        // http://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection (user: Cygon)
        public static bool AABBonCircle(AABBCollider box, CircleCollider circle)
        {
            Vector2 posCircle = circle._transform.Position;
            Vector2 posBox = box._transform.Position;
            float closestX = MathHelper.Clamp(posCircle.X, posBox.X + box.TopLeftCorner.X, posBox.X + box.DownRightCorner.X);
            float closestY = MathHelper.Clamp(posCircle.Y, posBox.Y + box.TopLeftCorner.Y, posBox.Y + box.DownRightCorner.Y);

            Vector2 distance = new Vector2(posCircle.X - closestX, posCircle.Y - closestY);

            return distance.LengthSquared() < (circle.Radius * circle.Radius);
        }

        // TODO: This function is not finished
        // Should also work with none Axis Aligned boxes
        // http://stackoverflow.com/questions/401847/circle-rectangle-collision-detection-intersection For reference (user: ShreevatsaR)
        public static bool AABBonCircle3(AABBCollider box, CircleCollider circle)
        {
            // Center position of circle is inside of the box
            if (PointOnAABB(box, circle._transform.Position))
                return true;

            // Edge of box has point in circle
            Vector2[] edges = new Vector2[4];
            edges[0] = box.TopRightCorner - box.TopLeftCorner;
            edges[1] = box.DownRightCorner - box.TopRightCorner;
            edges[2] = box.DownLeftCorner - box.DownRightCorner;
            edges[3] = box.TopLeftCorner - box.DownLeftCorner;

            // Y = slope * X + b
            // Y2 = slope2 * X + b2
            // mX + b = m2X + b2
            float squaredRadius = circle.Radius * circle.Radius;
            for (int i = 0; i < 4; i++)
            {
                Vector2 perpendicular = new Vector2(-edges[i].Y, edges[i].X);

                Vector2 center = circle.Center;
                Vector2 posBox = box._transform.Position;

                // Line Circle to Point
                float slope = nonInfinityDivideFloat(perpendicular.Y, perpendicular.X);
                float b = center.Y - slope * center.X;
                
                // Line Edge point to point
                float slope2 = nonInfinityDivideFloat(edges[i].Y, edges[i].X);
                float b2 = (edges[i].Y + posBox.Y)  - slope2 * (edges[i].X + posBox.X);

                float intersectionX = nonInfinityDivideFloat(b2 - b, (slope - slope2));
                float intersectionY = slope * intersectionX + b;
                // TODO: Point must be between the 2 corners
                Vector2 point = new Vector2(intersectionX, intersectionY);
                
                if (Vector2.DistanceSquared(center, point) <= squaredRadius)
                    return true;
            }
    
            return false;
        }

        /// <summary>
        /// Returns 0 if dividing by zero (instead of Infinity)
        /// </summary>
        /// <param name="a">value a</param>
        /// <param name="b">value b</param>
        /// <returns>Returning float value of A divided by B</returns>
        private static float nonInfinityDivideFloat(float a, float b)
        {
            return b != 0 ? a / b : 0;
        }

        // Arclight: Ferdy & Midas
        public static bool AABBonCircleArclight(AABBCollider box, CircleCollider circle)
        {
            Vector2 posCircle = circle._transform.Position;
            Vector2 posBox = box._transform.Position;

            if (PointOnAABB(box, posCircle + new Vector2(circle.Radius, 0))
                || PointOnAABB(box, posCircle + new Vector2(-circle.Radius, 0))
                || PointOnAABB(box, posCircle + new Vector2(0, circle.Radius))
                || PointOnAABB(box, posCircle + new Vector2(0, -circle.Radius)))
                return true;
            else if (PointOnCircle(circle, posBox + box.TopLeftCorner)
                || PointOnCircle(circle, posBox + box.TopRightCorner)
                || PointOnCircle(circle, posBox + box.DownLeftCorner)
                || PointOnCircle(circle, posBox + box.DownRightCorner))
                return true;
            else
                return false;
        }

        private static Vector2 closestPointOnCircle(Vector2 point, Vector2 circlePos, float radius)
        {
            Vector2 delta = point - circlePos;
            delta.Normalize();
            delta *= radius;
            delta += circlePos;
            return delta;
        }

        public static bool PointOnAABB(AABBCollider box, Vector2 point)
        {
            Vector2 boxMin = box._transform.Position + box.TopLeftCorner;
            Vector2 boxMax = box._transform.Position + box.DownRightCorner;

            return (point.X >= boxMin.X && point.X <= boxMax.X) &&
                (point.Y >= boxMin.Y && point.Y <= boxMax.Y);
        }

        public static bool PointOnCircle(CircleCollider circle, Vector2 point)
        {
            Vector2 delta = circle._transform.Position - point;
            return delta.Length() <= circle.Radius;
        }

    }
}
