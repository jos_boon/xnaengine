﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

// Engine
using XNAEngine;

namespace XNAEngine.Physics
{
    public class CircleCollider : Collider
    {
        private float _radius;

        public Vector2 Center
        {
            get { return _transform.Position; }
        }

        public float Radius
        {
            get { return _radius; }
            set
            {
                _radius = value;
                Halfsize = new Vector2(_radius, _radius);
            }
        }

        public CircleCollider(float radius, GameObject gObj, PhysicsWorld world) : base(gObj, world)
        {
            Radius = radius;
        }
    }
}
