﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using XNAEngine;
using XNAEngine.InputHandlers;

namespace XNAEngine.UI
{
    // TODO: Cleanup
    // TODO: Convert text to lowercase
    public class TextArea : UIComponent
    {
        private StringBuilder       _text;
        //private SpriteFont          _textFont;
        //private Vector2             _scrollPosition;

        private Vector2             _textSize;
        // State to see if text has been added since the last draw
        private bool                _hasChanged;

        public SpriteFont TextFont
        {
            get { return Font; }
            set 
            {
                Font = value;
                FontSize = Font.MeasureString("A"); // Measure the font size of a default character in pixels
                TabSize = Font.MeasureString("\t"); // Measure the font size of a tab in pixels
            }
        }

        public Vector2 FontSize
        {
            get;
            private set;
        }

        public Vector2 TabSize
        {
            get;
            private set;
        }

        public bool MultiLine;
        public bool ReadOnly;

        //TODO: Implement these
        // Behaviour Overflow visible(none) | hidden | h-scroll | v-scroll | h&v-scroll
        // Behaviour Word-Wrap <none> | break at length | break (no breaking on words)
        public int WrapAtLength = 100;

        public override string Text
        {
            get { return _text.ToString(); }
            set
            {
                // TODO: We're using a StringBuilder here. You use StrinBuilder.Append to add a string not Text += value. Think of way to convey this back to the programmer.
                throw new NotImplementedException("String can't be added through the Text property of a TextArea Object. Use Write or WriteLine to append your string.");
            }
        }

        public event EventHandler OnEnterPressed;

        public TextArea(Game game, Physics.PhysicsWorld world) : base(game, world)
        {
            MultiLine = false;
            ReadOnly = false;
            Focus = false;
            _text = new StringBuilder();
        }

        protected override void LoadContent()
        {
            // Set background color
            SetBackgroundTexture(new Color(0, 0, 0, 0));
            base.LoadContent();
        }
        
        public void Clear()
        {
            _text.Clear();

            // REMOVE: For some reason I thought we're working with .NET framework 2.0 which doesn't have clear function for StringBuilders
            //// fully clear the string builder
            //_text.Length = 0;
            //_text.Capacity = 0;

            //// Restore the to default capacity
            //_text.Capacity = 16;

            calcTextSize();
            _hasChanged = true;
        }

        public void Write(string s)
        {
            _text.Append(s);

            calcTextSize();
            _hasChanged = true;
        }

        public void WriteLine(string s)
        {
            if(MultiLine)
                _text.AppendLine(s);
            else
                Write(s);

            calcTextSize();
            _hasChanged = true;
        }

        private void calcTextSize()
        {
            _textSize = Font.MeasureString(_text);
        }

        public override void Update(GameTime gameTime)
        {
            if(Focus)
            {
                handleMouseInput();
                handleKeyboardInput();
            }

            base.Update(gameTime);
        }

        private void handleMouseInput()
        {
            if(_mouse.ScrollUp)
            {
                _scrollPosition.Y += 5;
            }
            else if(_mouse.ScrollDown)
            {
                _scrollPosition.Y -= 5;
            }
           
            Vector2 min = Vector2.Zero;

            if (_textSize.Y > Size.Y)
            {
                min.Y = -_textSize.Y + Size.Y;
            }


            _scrollPosition = Vector2.Clamp(_scrollPosition, min, Vector2.Zero);
        }

        private void handleKeyboardInput()
        {
            if (!ReadOnly)
            {
                if (_keyboard.getExtendedKeyState(Keys.Enter) == ExtendButtonState.JustReleased)
                {
                    if (OnEnterPressed != null)
                        OnEnterPressed(this, null);
                }
                else if ((_keyboard.getExtendedKeyState(Keys.Back) == ExtendButtonState.JustReleased)
                && _text.Length > 0)
                {
                    _text.Length -= 1;
                }

                if (_keyboard.HasKeyboardStateChanged)
                {
                    // TODO: Typing feels weird(possible to get twice the same character on one click)
                    // TODO: Some keys code don't match the unicode of the spritefont :(
                    Keys[] keys = _keyboard.GetPressedKeys;
                    // Font goes from 32 to 254
                    if (keys.Length > 0 && (int)keys[0] >= 32)
                    {
                        _text.Append(ConvertKey(keys[0]));
                    }
                        
                }
            }
        }

        // TODO: add conversion for other keys that aren't mathing
        // XNA Input keys doesn't match the ASCII codes
        private char ConvertKey(Keys k)
        {
            if (k == Keys.OemMinus) // DASH (-)
                return (char)Keys.Insert;

            return (char)k;
        }

        public override void Draw(GameTime gameTime)
        {
            if(_text.Length > 0)
            {
                if (_hasChanged && WrapAtLength > 0 && MultiLine)
                    WordWrap();
            }

            // Reset state
            _hasChanged = false;

            base.Draw(gameTime);
        }

        // TODO: Expand to take in account of not breaking words up (unless the word is just to long)
        // TODO: Support dynamic sizing of the textarea
        // TODO: WordWrap messes up the textAlign
        private void WordWrap()
        {
            string pattern = "\n|\t";
            string s = _text.ToString() + "\n"; // Making sure the string ends with an end line

            MatchCollection matches = Regex.Matches(s, pattern);
            int iOffset = 0;
            int prevNewLine = 0;

            for (int i = 0; i < matches.Count; i++)
            {
                int tab = 0;
                int j = i;

                // Count the tabs and look for the next newline ocurrence
                for (; j < matches.Count - 1; j++)
                {
                    if (matches[j].Value == "\t")
                        tab++;
                    else
                        break;
                }

                int endLineIndex = matches[j].Index - 1;
                int startLineIndex = i > 0 ? matches[prevNewLine].Index + 1 : 0;

                int lineLength = endLineIndex - startLineIndex;
                int finalWrapLength = WrapAtLength - (int)(TabSize.X * tab / FontSize.X);

                for (; lineLength > finalWrapLength; lineLength -= finalWrapLength)
                {
                    startLineIndex += finalWrapLength;
                    _text.Insert(startLineIndex + iOffset, '\n');
                    iOffset++;
                }

                prevNewLine = j;
                i += tab;
            }

            // So it can properly scroll afterwards
            calcTextSize();
        }
    }
}
