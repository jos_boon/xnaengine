﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using XNAEngine;
using XNAEngine.Physics;

namespace XNAEngine.UI
{
    // TODO: Make scrollable
    public class Panel : UIComponent
    {
        public Panel(Game game, PhysicsWorld world)
            : base(game, world)
        {

        }

        public override void Initialize()
        {
            collider = new AABBCollider(Size / 2, this, _world);

            base.Initialize();
        }

        public void AddUIComponent(UIComponent component)
        {
            if (!Children.Exists(x => x == component))
                component.Parent = this;
        }

        public void RemoveUIComponent(UIComponent component)
        {
            if (Children.Exists(x => x == component))
                component.Parent = null;
        }
    }
}
