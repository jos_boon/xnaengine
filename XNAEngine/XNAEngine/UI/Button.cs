﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

using XNAEngine;
using XNAEngine.Physics;

namespace XNAEngine.UI
{
    public class Button : UIComponent
    {
        // TODO: If no sprite (animation) supplied THEN create own sprites for button animation
        public Button(Game game, PhysicsWorld world) : base (game, world)
        {
            Size = new Vector2(100, 50);
            Text = "Button";
            textAlign = TextAlign.Center | TextAlign.Middle;

        }

        public override void Initialize()
        {
            collider = new AABBCollider(Size / 2, this, _world);

            collider.AttachMouseEvent(btn_Hold, Collider.MouseEventType.OnMouseHold);
            collider.AttachMouseEvent(btn_Hover, Collider.MouseEventType.OnMouseHover);
            collider.AttachMouseEvent(btn_Exit, Collider.MouseEventType.OnMouseExit);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            btn_Exit(this, null);

            base.LoadContent();
        }

        #region Button Animation
        private void btn_Hold(object sender, EventArgs e)
        {
            SetBackgroundTexture(Color.DarkGoldenrod);
        }

        private void btn_Hover(object sender, EventArgs e)
        {
            SetBackgroundTexture(Color.LightGoldenrodYellow);
        }

        private void btn_Exit(object sender, EventArgs e)
        {
            SetBackgroundTexture(Color.Goldenrod);
        }
        #endregion
    }
}
