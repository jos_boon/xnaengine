﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using XNAEngine;
using XNAEngine.Physics;
using XNAEngine.InputHandlers;

namespace XNAEngine.UI
{
    // TODO: Text font size (through scaling)
    // TODO: Padding/Margin
    // TODO: Load in default font (low)

    public abstract class UIComponent : GameObject
    {
        protected MouseHandler      _mouse;
        protected KeyboardHandler   _keyboard;

        public bool                 Focus;

        public Color                FillColor;
        public Color                BorderColor;
        public int                  BorderSize;
        public Vector2              Size;

        public SpriteFont           Font;
        public Color                FontColor;
        public virtual string       Text { get; set; }
        public TextAlign            textAlign; // TODO: Property
        private Vector2             _textOrigin;

        public bool                 ScrollAble;
        protected Vector2           _scrollPosition;

        public UIComponent(Game game, PhysicsWorld world)
            : base(game, world)
        {
            _keyboard = InputManager.keyboardHandler;
            _mouse = InputManager.mouseHandler;

            FillColor = Color.Transparent;
            BorderColor = Color.Transparent;
            BorderSize = 0;
            Size = new Vector2(400, 400);

            _textOrigin = Vector2.Zero;
            textAlign = TextAlign.Left | TextAlign.Top;
            FontColor = Color.White;
            ScrollAble = false;
            _scrollPosition = Vector2.Zero;
        }

        public override void Initialize()
        {
            sprite = new Sprite(this);
            sprite.Initialize();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            if(sprite.texture == null)
                SetBackgroundTexture(Color.LightGray);

            // TODO: Load in default font
            if(Font == null)
                Font = XNAEngineContentLib.Content.Load<SpriteFont>("defaultFont"); ;

            base.LoadContent();
        }

        // TODO: This method might take up to much time....
        // TODO: Use DrawPrimitives.CreateQuadrilateral
        public void SetBackgroundTexture(Color color)
        {
            Texture2D background = new Texture2D(Game.GraphicsDevice, (int)Size.X, (int)Size.Y);
            Color[] c = new Color[background.Width * background.Height];

            for (int i = 0; i < c.Length; i++)
                c[i] = color;

            background.SetData<Color>(c, 0, c.Length);

            sprite.ChangeTexture(background);
        }

        // TODO: Align Text and creating Clipping rectangle now happens every Draw call.
        public override void Draw(GameTime gameTime)
        {
            // Set Clipping Rectangle
            Rectangle rec = Game.GraphicsDevice.ScissorRectangle;
            Rectangle clip = FitScissorRectangle(sprite.DestinationRectangle, rec);

            _batch.GraphicsDevice.ScissorRectangle = clip;

            // Draw Background Texture
            base.Draw(gameTime);

            // Draw Text
            if(!String.IsNullOrEmpty(Text))
            {
                
                AlignText();
                Vector2 TextPos = transform.Position;
                if (ScrollAble)
                    TextPos += _scrollPosition;

                _batch.DrawString(Font, Text, TextPos, FontColor, 0, _textOrigin, 1, SpriteEffects.None, LayerDepthOrder.DefaultLayer.Value);
            }

            // Set Clipping Rectangle back to Original
            _batch.GraphicsDevice.ScissorRectangle = rec;
        }

        public void AlignText()
        {
            Vector2 textSize = Font.MeasureString(Text);

            if ((textAlign & TextAlign.Left) == TextAlign.Left)
                _textOrigin.X = Size.X / 2;
            if ((textAlign & TextAlign.Right) == TextAlign.Right)
                _textOrigin.X = -Size.X / 2 + textSize.X;
            if ((textAlign & TextAlign.Center) == TextAlign.Center)
                _textOrigin.X = textSize.X / 2;

            if ((textAlign & TextAlign.Top) == TextAlign.Top)
                _textOrigin.Y = Size.Y / 2;
            if ((textAlign & TextAlign.Bottom) == TextAlign.Bottom)
                _textOrigin.Y = -Size.Y / 2 + textSize.Y;
            if ((textAlign & TextAlign.Middle) == TextAlign.Middle)
                _textOrigin.Y = textSize.Y / 2;
        }

        private Rectangle FitScissorRectangle(Rectangle clip, Rectangle fit)
        {
            clip.X -= (int)Size.X / 2;
            clip.Y -= (int)Size.Y / 2;

            if (clip.X < fit.X)
            {
                clip.Width = clip.X > clip.Width ? 0 : clip.Width + clip.X;
                clip.X = fit.X;
            }
            else if (clip.X > fit.Width)
                clip.X = fit.Width;

            if (clip.Y < fit.Y)
            {
                clip.Height = clip.Y > clip.Height ? 0 : clip.Height + clip.Y;
                clip.Y = fit.Y;
            }
            else if (clip.Y > fit.Height)
                clip.Y = fit.Height;

            if (clip.X + clip.Width > fit.Width)
                clip.Width = fit.Width - clip.X;

            if (clip.Y + clip.Height > fit.Height)
                clip.Height = fit.Height - clip.Y;

            return clip;
        }
    }

    [Flags]
    public enum TextAlign
    {
        Left    = 1,
        Center  = 2,
        Right   = 4,
        Top     = 8,
        Middle  = 16,
        Bottom  = 32
    }
}
